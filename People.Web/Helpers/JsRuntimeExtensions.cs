﻿using Microsoft.JSInterop;
using System.Threading.Tasks;

namespace People.Web.Helpers
{
    public static class JsRuntimeExtensions
    {
        public async static ValueTask<string> GetSessionStorageItem(this IJSRuntime js, string key)
        {
            try
            {
                return await js.InvokeAsync<string>("sessionStorage.getItem", key);
            }
            catch
            {
                return null;
            }
        }
        public async static ValueTask SetSessionStorageItem(this IJSRuntime js, string key, string value) => await js.InvokeVoidAsync("sessionStorage.setItem", key, value);

        public async static ValueTask RemoveSessionStorageItem(this IJSRuntime js, string key) => await js.InvokeVoidAsync("sessionStorage.removeItem", key);

        public async static ValueTask Log(this IJSRuntime js, object value) => await js.InvokeVoidAsync("console.log", value);
    }
}
