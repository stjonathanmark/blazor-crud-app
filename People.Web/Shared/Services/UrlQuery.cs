﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;

namespace People.Web.Shared.Services
{
    public class UrlQuery : IUrlQuery
    {
        protected Dictionary<string, StringValues> parameters;

        public UrlQuery(NavigationManager navMgr)
        {
            parameters = QueryHelpers.ParseQuery(new Uri(navMgr.Uri).Query);
        }

        public string GetValue(string paramName)
        {
            if (parameters == null || !parameters.ContainsKey(paramName)) return string.Empty;

            var value = parameters[paramName];

            return value;
        }

        public T GetValue<T>(string paramName)
        {
            var value = GetValue(paramName);

            if (string.IsNullOrEmpty(value)) return default;

            return (T)Convert.ChangeType(value, typeof(T));
        }
    }
}
