﻿using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;
using People.Dto;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace People.Web.Shared.Services
{

    public class PersonService : BaseService, IPersonService
    {

        private readonly HttpClient http;
        

        public PersonService(HttpClient httpClient)
        {
            http = httpClient;
        }

        
        public async Task<CreatePersonResponse> CreatePerson(CreatePersonRequest request)
        {
            return await http.PostJsonAsync<CreatePersonResponse>("api/person", request);
        }

        public async Task<DeletePersonResponse> DeletePerson(DeletePersonRequest request)
        {
            var httpResponse = await http.DeleteAsync($"api/person/{request.PersonId}");

            var json = await httpResponse.Content.ReadAsStringAsync();

            var response = JsonConvert.DeserializeObject<DeletePersonResponse>(json);

            return response;
        }

        public async Task<GetPeopleResponse> GetPeople(GetPeopleRequest request)
        {
            var queryString = GetPagingQuery(request);
            return await http.GetJsonAsync<GetPeopleResponse>($"api/person{queryString}");
        }

        public async Task<GetPersonResponse> GetPerson(GetPersonRequest request)
        {
            return await http.GetJsonAsync<GetPersonResponse>($"api/person/{request.PersonId}");
        }

        public async Task<SearchPeopleResponse> SearchPeople(SearchPeopleRequest request)
        {
            var queryProps = new Dictionary<string, string>()
            {
                { "Prefix", "prefix" },
                { "Suffix", "suffix" },
                { "FirstName", "firstName" },
                { "MiddleName", "middleName" },
                { "LastName", "lastName" },
                { "Gender", "gender" },
                { "StartBirthDate", "startBirthDate" },
                { "EndBirthDate", "endBirthDate" }

            };
            var queryString = GetQuery(request, queryProps);
            return await http.GetJsonAsync<SearchPeopleResponse>($"api/person/{queryString}");
        }

        public async Task<UpdatePersonResponse> UpdatePerson(UpdatePersonRequest request)
        {
            return await http.PutJsonAsync<UpdatePersonResponse>($"api/person/{request.PersonId}", request);
        }
    }
}
