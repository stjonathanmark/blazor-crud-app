﻿using People.Dto;
using System.Threading.Tasks;

namespace People.Web.Shared.Services
{
    public interface IPersonService
    {
        Task<CreatePersonResponse> CreatePerson(CreatePersonRequest request);

        Task<GetPeopleResponse> GetPeople(GetPeopleRequest request);

        Task<GetPersonResponse> GetPerson(GetPersonRequest request);

        Task<SearchPeopleResponse> SearchPeople(SearchPeopleRequest request);

        Task<UpdatePersonResponse> UpdatePerson(UpdatePersonRequest request);

        Task<DeletePersonResponse> DeletePerson(DeletePersonRequest request);
    }
}
