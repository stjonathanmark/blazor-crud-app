﻿
namespace People.Web.Shared.Services
{
    public interface IUrlQuery
    {
        string GetValue(string paramName);

        T GetValue<T>(string paramName);
    }
}
