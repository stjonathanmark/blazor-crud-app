﻿using People.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace People.Web.Shared.Services
{
    public abstract class BaseService
    {
        protected string GetPagingQuery(BaseDataRequest request)
        {
            var query = new StringBuilder();

            if (request.PageItems)
            {
                query.Append($"{(query.Length > 0 ? "?" : string.Empty)}pageItems=true");
                if (request.PageNumber != 1) query.Append($"&pageNumber={request.PageNumber.Value}");
                if (request.PageSize != 10) query.Append($"&pageSize={request.PageSize.Value}");
                if (request.MaxPages != 5) query.Append($"&pageSize={request.MaxPages}");
            }

            for(var i = 0; i < request.OrderBys.Count; i++)
            {
                string startingChar = query.Length > 0 ? "&" : "?";
                query.Append($"{startingChar}orderBys[{i}]={request.OrderBys[i]}");
            }

            if (request.IncludeDeleted)
            {
                string startingChar = query.Length > 0 ? "&" : "?";
                query.Append($"{startingChar}includeDeleted=true");
            }

            return query.ToString();
        }

        protected string GetQuery<TRequest>(TRequest request, Dictionary<string, string> queryProps)
            where TRequest : BaseRequest
        {
            var query = new StringBuilder();

            if (request is BaseDataRequest) query.Append(GetPagingQuery((BaseDataRequest)Convert.ChangeType(request, typeof(BaseDataRequest))));

            var props = request.GetType().GetProperties();

            foreach (var prop in props)
            {
                var value = prop.GetValue(request);
                if (queryProps.ContainsKey(prop.Name) && value != null)
                {
                    string startingChar = query.Length > 0 ? "&" : "?";
                    query.Append($"{startingChar}{queryProps[prop.Name]}={prop.GetValue(request)}");
                }
            }

            return query.ToString();
        }
    }
}
