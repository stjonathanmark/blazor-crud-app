﻿using dm = People.Web.Models;
using People.Dto;
using System.Threading.Tasks;

namespace People.Web.Auth
{
    public interface ILoginService
    {
        Task<AuthenticateUserResponse> Login(AuthenticateUserRequest request);

        Task Logout();
    }
}
