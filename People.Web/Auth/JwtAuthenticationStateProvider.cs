﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.JSInterop;
using People.Dto;
using People.Web.Helpers;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;

namespace People.Web.Auth
{
    public class JwtAuthenticationStateProvider : AuthenticationStateProvider, ILoginService
    {
        private readonly string key = "token-key";
        private readonly IJSRuntime js;
        private readonly HttpClient http;

        public JwtAuthenticationStateProvider(IJSRuntime jsAdapter, HttpClient httpClient) 
        {
            js = jsAdapter;
            http = httpClient;
        }

        public async override Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            var token = await js.GetSessionStorageItem(key);

            var state = BuildAuthenticationState(token);

            return state;
        }

        public async Task<AuthenticateUserResponse> Login(AuthenticateUserRequest credentials)
        {
            var response = await http.PostJsonAsync<AuthenticateUserResponse>("api/account/authenticate", credentials);
            if (response.Successful)
            {
                if (response.Authenticated)
                {
                    await js.SetSessionStorageItem(key, response.Token);
                    NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());
                }
            }

            return response;
        }

        public async Task Logout()
        {
            http.DefaultRequestHeaders.Authorization = null;
            await js.RemoveSessionStorageItem(key);
            NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());
        }

        private AuthenticationState BuildAuthenticationState(string token)
        {
            ICollection<Claim> claims = null;
            var authType = string.IsNullOrEmpty(token) ? null : "jwt";

            if (!string.IsNullOrEmpty(authType))
            {
                http.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var handler = new JwtSecurityTokenHandler();
                var tokenObj = handler.ReadJwtToken(token);
                claims = tokenObj.Claims.ToList();
                var role = claims.First(c => c.Type == "role").Value;
                claims.Add(new Claim(ClaimTypes.Role, role));
            }
            
            return new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity(claims, authType)));
        }
    }
}
