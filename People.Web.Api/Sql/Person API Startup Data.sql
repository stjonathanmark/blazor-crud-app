INSERT INTO Roles 
	(Deleted, [Name], [Description])
VALUES
	(0, 'Administrator', 'A user with full authorization'),
	(0, 'User', 'A user that has limited authorization');

INSERT INTO Persons 
	(Deleted, FirstName, MiddleName, LastName, Gender, BirthDate)
VALUES
	(0, 'Jonathan', 'Mark', 'Mortimer', 'Male', '1983-09-03');