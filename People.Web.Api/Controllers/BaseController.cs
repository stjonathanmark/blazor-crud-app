﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using People.Data;
using People.Security;
using People.Dto;
using People.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading.Tasks;

namespace People.Web.Api.Controllers
{
    public abstract class BaseController : ControllerBase
    {
        protected readonly DataContext db;

        public BaseController(DataContext context)
        {
            db = context;
        }

        protected long UserId => GetClaimValue<long>(ClaimNames.UserId);

        protected string Username => GetClaimValue(ClaimNames.Username);

        public string Role => GetClaimValue(ClaimTypes.Role);

        protected string Subject => GetClaimValue(ClaimNames.Subject);

        protected string Name => GetClaimValue(ClaimNames.Name);

        protected bool UserIsAdministrator => Role.ToLower() == "administrator";

        protected void HandleException(BaseResponse response, Exception exception, string message)
        {
            GetExceptionInfo(exception, response, message);
        }

        protected long ItemsFound { get; set; }

        protected IPagingInfo GetPagingInfo(BaseDataRequest request, long itemCount)
        {
            Pager pi = null;

            if (request.PageItems)
            {
                pi = new Pager(itemCount, request.PageNumber.Value, request.PageSize.Value, request.MaxPages);
            }

            return pi;
        }

        protected void GetExceptionInfo(Exception exception, BaseResponse response, string defaultMsg)
        {
            response.Successful = false;
            response.Message = defaultMsg;

            if (UserIsAdministrator)
            {
                int counter = 1;
                Exception ex = exception;

                do
                {
                    response.SetDetail("Level " + counter.ToString() + " Message", ex.Message);
                    response.SetDetail("Level " + counter.ToString() + " Stack Trace", ex.StackTrace);
                    ex = ex.InnerException;
                    counter++;

                } while (ex != null);
            }
        }

        protected string GetClaimValue(string type)
        {
            if (User == null || User.Claims == null || !User.Claims.Any()) return string.Empty;

            var claim = User.Claims.FirstOrDefault(c => c.Type == type);

            return claim != null ? claim.Value : string.Empty;
        }

        protected T GetClaimValue<T>(string type)
        {
            var value = GetClaimValue(type);
            return string.IsNullOrEmpty(value) ? default : (T)Convert.ChangeType(value, typeof(T));
        }

        protected async Task<IList<TEntity>> SelectAsync<TEntity>(Expression<Func<TEntity, bool>> predicate = null, ICollection<string> orderBys = null, int? skip = null, int? take = null, bool includeDeleted = false)
            where TEntity : DataEntity
        {
            ItemsFound = 0;

            var set = db.Set<TEntity>().Where(GetDeleted<TEntity>(includeDeleted));

            ItemsFound = await ((predicate != null) ? set.Where(predicate).CountAsync() : set.CountAsync());

            var query = GetQuery(set, predicate, skip, take, orderBys);

            return await query.ToListAsync();
        }

        protected async Task<IList<TEntity>> SelectAsync<TEntity>(string predicate = "", ICollection<string> orderBys = null, int? skip = null, int? take = null, bool includeDeleted = false)
            where TEntity : DataEntity
        {
            ItemsFound = 0;

            var set = db.Set<TEntity>().Where(GetDeleted<TEntity>(includeDeleted));

            ItemsFound = await ((predicate != string.Empty) ? set.Where(predicate).CountAsync() : set.CountAsync());

            var query = GetQuery(set, predicate, skip, take, orderBys);

            return await query.ToListAsync();
        }

        private IQueryable<TEntity> GetQuery<TEntity>(IQueryable<TEntity> set, string predicate, int? skip, int? take, ICollection<string> orderBys)
            where TEntity : DataEntity
        {
            var query = predicate != string.Empty ? set.Where(predicate) : set;
            return GetQuery(query, skip, take, orderBys);
        }

        private IQueryable<TEntity> GetQuery<TEntity>(IQueryable<TEntity> set, Expression<Func<TEntity, bool>> predicate, int? skip, int? take, ICollection<string> orderBys)
            where TEntity : DataEntity
        {
            var query = predicate != null ? set.Where(predicate) : set;
            return GetQuery(query, skip, take, orderBys);
        }

        private IQueryable<TEntity> GetQuery<TEntity>(IQueryable<TEntity> query, int? skip, int? take, ICollection<string> orderBys)
        where TEntity : DataEntity
        {

            if (skip.HasValue)
            {
                query = query.Skip(skip.Value);
            }

            if (take.HasValue)
            {
                query = query.Take(take.Value);
            }

            if (orderBys != null && orderBys.Any())
            {
                var first = true;

                foreach (var clause in orderBys)
                {
                    query = first
                        ? query.OrderBy(clause)
                        : ((IOrderedQueryable<TEntity>)query).ThenBy(clause);

                    first = false;
                }
            }

            return query;
        }

        protected Expression<Func<T, bool>> GetDeleted<T>(bool includeDeleted)
            where T : DataEntity
        {
            Expression<Func<T, bool>> exp = entity => !entity.Deleted;

            return includeDeleted ? null : exp;
        }
    }
}
