﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using People.Dto;
using People.Data;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace People.Web.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : BaseController
    {
        private readonly IMapper mapper;

        public PersonController(DataContext context, IMapper mapper) 
            : base(context)
        {
            this.mapper = mapper;
        }

        // Body Properties
        // 1) prefix: [string] - Value used to search by prefix
        // 4) lastName: [string] - Value used to search by last name - required
        // 5) suffix: [string] - Value used to search by suffix
        // 2) firstName: [string] - Value used to search by first name - required
        // 3) middleName: [string] - Value used to search by middle name
        // 6) gender: [female|male] - Value used to search by first name ('female' or 'male' are the only acceptable values) - required
        // 7) birthDate: [date] - Value used to search by first name (date must by in format 'yyyy-mm-dd') - required
        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public async Task<CreatePersonResponse> Post(CreatePersonRequest request)
        {
            var response = new CreatePersonResponse();

            try
            {
                var person = GetPerson(request);

                await db.AddAsync(person);
                await db.SaveChangesAsync();

                response.Successful = true;
                response.Message = request.DefaultSuccessMessage;
            }
            catch (Exception exception)
            {
                HandleException(response, exception, request.DefaultErrorMessage);
            }

            return response;
        }

        // Optional query params:
        // 1) pageItems=[true|false] - Whether to page items ('true' or 'false' are the only acceptable values) 
        // 2) pageSize=[integer] - How many items per page - default: 10 (only applicable when pageItems is set to 'true')
        // 3) pageNumber=[integer] - Which page of items to return - default: 1 (only applicable when pageItems is set to 'true')
        // 4) maxPages=[integer] - How many paging links/buttons to show - default: 5 (only applicable when pageItems is set to 'true')
        [HttpGet]
        public async Task<GetPeopleResponse> Get([FromQuery] GetPeopleRequest request)
        {
            var response = new GetPeopleResponse();

            try
            {
                Expression<Func<Person, bool>> predicate = null;
                response.People = mapper.Map<IEnumerable<PersonDto>>(await SelectAsync(predicate, request.OrderBys, request.Skip, request.Take, request.IncludeDeleted));
                response.PagingInfo = GetPagingInfo(request, ItemsFound);
                response.Successful = true;
                response.Message = request.DefaultSuccessMessage;
            }
            catch(Exception exception)
            {
                HandleException(response, exception, request.DefaultErrorMessage);
            }

            return response;
        }

        // Optional query params:
        //  1) pageItems=[true|false] - Whether to page items ('true' or 'false' are the only acceptable values)
        //  2) pageSize=[integer] - How many items per page
        //  3) pageNumber=[integer] - Which page of items to return
        //  4) maxPages=[integer] - How many paging links/buttons to show
        //  5) prefix=[string] - Value used to search by prefix
        //  6) firstName=[string] - Value used to search by first name
        //  7) middleName=[string] - Value used to search by middle name
        //  8) lastName=[string] - Value used to search by last name
        //  9) suffix=[string] - Value used to search by suffix
        // 10) gender=[female|male] - Value used to search by gender ('female' or 'male' are the only acceptable values)
        // 11) startBirthDate=[date] - Value used to search by birth date equal to and greater (date must by in format 'yyyy-mm-dd')
        // 12) endBirthDate=[date] - Value used to search by birth date equal to and less thant (date must by in format 'yyyy-mm-dd')
        [Route("Search")]
        [HttpGet]
        public async Task<SearchPeopleResponse> Get([FromQuery] SearchPeopleRequest request)
        {
            var response = new SearchPeopleResponse();

            try
            {
                response.People = mapper.Map<IEnumerable<PersonDto>>(await SelectAsync<Person>(GetWhereClause(request), request.OrderBys, request.Skip, request.Take, request.IncludeDeleted));
                response.PagingInfo = GetPagingInfo(request, ItemsFound);
                response.Successful = true;
                response.Message = request.DefaultSuccessMessage;
            }
            catch (Exception exception)
            {
                HandleException(response, exception, request.DefaultErrorMessage);
            }

            return response;
        }

        // Route Parameter
        // 1) PersonId - Integer that is the id of the person to retreive
        [Route("{PersonId}")]
        [HttpGet]
        public async Task<GetPersonResponse> Get([FromRoute] GetPersonRequest request)
        {
            var response = new GetPersonResponse();

            try
            {
                response.Person = mapper.Map<PersonDto>(await db.Persons.FirstOrDefaultAsync(p => p.Id == request.PersonId && !p.Deleted));
                response.Successful = true;
                response.Message = request.DefaultSuccessMessage;
            }
            catch(Exception exception)
            {
                HandleException(response, exception, request.DefaultErrorMessage);
            }

            return response;
        }

        // Route Parameter
        // 1) PersonId - Integer that is the id of the person to update
        // Body Properties
        // 1) prefix: [string] - Value used to search by prefix
        // 4) lastName: [string] - Value used to search by last name - required
        // 5) suffix: [string] - Value used to search by suffix
        // 2) firstName: [string] - Value used to search by first name - required
        // 3) middleName: [string] - Value used to search by middle name
        // 6) gender: [female|male] - Value used to search by first name ('female' or 'male' are the only acceptable values) - required
        // 7) birthDate: [date] - Value used to search by first name (date must by in format 'yyyy-mm-dd') - required
        [HttpPut]
        [Route("{PersonId}")]
        public async Task<UpdatePersonResponse> Put([FromRoute] int personId, [FromBody] UpdatePersonRequest request)
        {
            var response = new UpdatePersonResponse();

            try { 
                var person = GetPerson(request);
                person.Id = personId;

                db.Update(person);
                await db.SaveChangesAsync();

                response.Successful = true;
                response.Message = request.DefaultSuccessMessage;
            }
            catch (Exception exception)
            {
                HandleException(response, exception, request.DefaultErrorMessage);
            }

            return response;
        }

        // Route Parameter
        // 1) PersonId - Integer that is the id of the person to delete
        [Authorize(Roles = "Administrator")]
        [HttpDelete]
        [Route("{PersonId}")]
        public async Task<DeletePersonResponse> Delete([FromRoute] DeletePersonRequest request)
        {
            var response = new DeletePersonResponse();

            try
            {
                var person = await db.Persons.FirstOrDefaultAsync(p => p.Id == request.PersonId && !p.Deleted);

                db.Remove(person);
                await db.SaveChangesAsync();

                response.Successful = true;
                response.Message = request.DefaultSuccessMessage;
            }
            catch (Exception exception)
            {
                HandleException(response, exception, request.DefaultErrorMessage);
            }

            return response;
        }

        private Person GetPerson(CreatePersonRequest request)
        {
            return new Person()
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                MiddleName = request.MiddleName,
                Prefix = request.Prefix,
                Suffix = request.Suffix,
                Gender = request.Gender,
                BirthDate = request.BirthDate
            };
        }

        private string GetWhereClause(SearchPeopleRequest request)
        {
            var searchColumns = new List<string>()
            {
                "Suffix",
                "FirstName",
                "MiddleName",
                "LastName",
                "Prefix",
                "Gender",
                "StartBirthDate",
                "EndBirthDate"
            };

            StringBuilder whereClause = new StringBuilder();

            foreach (var prop in request.GetType().GetProperties())
            {
                var propName = prop.Name;
                var value = prop.GetValue(request);
                if (!searchColumns.Contains(propName) || value == null) continue;

                var column = propName.Contains("BirthDate") ? "BirthDate" : propName;

                var clause = string.Empty;
                if (propName == "Gender")
                    clause = $"{column} = \"{value.ToString()}\"";
                else if (propName == "StartBirthDate")
                    clause = $"{column} >= \"{((DateTime)value).ToString("yyyy-MM-dd")}\"";
                else if (propName == "EndBirthDate")
                    clause = $"{column} <= \"{((DateTime)value).ToString("yyyy-MM-dd")}\"";
                else
                    clause = $"{column}.Contains(\"{value.ToString()}\")";

                if (whereClause.Length > 0) whereClause.Append(" AND ");

                whereClause.Append(clause);
            }

            return whereClause.ToString();
        }
    }
}