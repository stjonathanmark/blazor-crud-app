using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using People.Data;
using People.Dto;
using People.Security;
using People.Security.Authentication;
using System.Collections.Generic;
using System.Text;

namespace People.Web.Api
{
    public class DefaultMappingProfile : Profile
    {
        public DefaultMappingProfile()
        {
            CreateMap<PersonDto, Person>();
            CreateMap<Person, PersonDto>();

            CreateMap<UserDto, User>();
            CreateMap<User, UserDto>();

            CreateMap<RoleDto, Role>();
            CreateMap<Role, RoleDto>();
        }
    }

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvcCore();
            services.AddControllers();
            services.AddAutoMapper(typeof(DefaultMappingProfile));

            services.AddSwaggerGen(opts =>
            {
                opts.SwaggerDoc("v1", new OpenApiInfo { Title = "Person API", Version = "v1" });
                opts.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Bearer Token - Format: Bearer {token}",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });
                opts.AddSecurityRequirement(new OpenApiSecurityRequirement 
                {
                    { 
                        new OpenApiSecurityScheme
                        { 
                            Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "Bearer" }
                        }, 
                        new List<string>() 
                    }
                });
            });

            services.AddAuthentication(opts =>
            {
                opts.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                opts.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
           .AddJwtBearer(opts =>
           {
               opts.TokenValidationParameters = new TokenValidationParameters()
               {
                   ValidateIssuer = false,
                   ValidateAudience = false,
                   ValidateLifetime = true,
                   ValidateIssuerSigningKey = true,

                   ValidIssuer = Configuration["Security:TokenIssuer"],
                   ValidAudience = Configuration["Security:TokenAudience"],
                   IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Security:TokenKey"]))
               };
           });

            services.AddDbContext<DataContext>(opts => opts.UseSqlServer(Configuration.GetConnectionString("DataContext")));
            services.AddTransient<IPasswordHasher, PasswordHasher>();
            services.AddTransient<IPasswordHashSettings, SecuritySettings>();
            services.AddTransient<ISecuritySettings, SecuritySettings>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var swaggerOpts = new SwaggerOptions();
            Configuration.GetSection(nameof(SwaggerOptions)).Bind(swaggerOpts);

            app.UseSwagger(opts => 
            {
                opts.RouteTemplate = swaggerOpts.JsonRoute;
            });

            app.UseSwaggerUI(opts =>
            {
                opts.SwaggerEndpoint(swaggerOpts.UiEndPoint, swaggerOpts.Description);
            });

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }

    public class SwaggerOptions
    {
        public string JsonRoute { get; set; }

        public string Description { get; set; }

        public string UiEndPoint { get; set; }
    }
}
