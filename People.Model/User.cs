﻿using People.Security;

namespace People
{
    public class User : BaseUser
    {
        public Role Role { get; }
    }
}
