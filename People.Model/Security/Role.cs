﻿using System.Collections.Generic;

namespace People.Security
{
    public class Role : BaseRole
    {
        public IList<User> Users { get; set; }
    }
}
