﻿using System.IO;
using System.Security.Claims;

namespace People.Security
{
    public class ClaimNames
    {
        public const string Subject = ClaimTypes.NameIdentifier;
        public const string Name = "name";
        public const string UserId = "uid";
        public const string Username = "usr";
        public const string Roles = "Roles";
    }
}
