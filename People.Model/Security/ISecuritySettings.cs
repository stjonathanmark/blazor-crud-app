﻿using People.Security.Authentication;

namespace People.Security
{
    public interface ISecuritySettings : IAuthenticationSettings, IPasswordHashSettings
    {
    }
}
