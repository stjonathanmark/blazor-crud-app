﻿using System.Text.RegularExpressions;

namespace People.Security.Authentication
{
    public interface IAuthenticationSettings : ITokenSettings, IPasswordChangeSettings
    {
        bool EnforceUsernameFormat { get; set; }

        string UsernameRegexPattern { get; set; }

        Regex UsernameRegex { get; }
    }
}
