﻿namespace People.Security.Authentication
{
    public interface IPasswordChangeSettings : IPasswordSettings 
    {
        bool LimitPasswordReuse { get; set; }

        ushort DaysBeforePasswordReuse { get; set; }

        byte ChangesBeforePasswordReuse { get; set; }
    }
}
