﻿namespace People.Security.Authentication
{
    public interface IPasswordHasher
    {
        string Hash(string password, string salt = default);

        string GenerateSalt();
    }
}
