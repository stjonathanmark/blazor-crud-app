﻿namespace People.Security.Authentication
{
    public interface ITokenSettings
    {
        bool UseTokens { get; set; }

        bool TokensExpire { get; set; }

        string TokenKey { get; set; }

        string Issuer { get; set; }

        string Audience { get; set; }

        int ExpirationTime { get; set; }
    }
}
