﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace People.Security.Authentication
{
    public class PasswordHasher : IPasswordHasher
    {
        private readonly IPasswordHashSettings settings;

        public PasswordHasher(IPasswordHashSettings passwordHashSettings)
        {
            settings = passwordHashSettings;
        }

        public string Hash(string password, string salt = default)
        {
            if (!settings.HashPasswords) return password;

            if (!string.IsNullOrWhiteSpace(salt) && settings.UseSaltedPasswords)
                password += salt;

            var originalBytes = Encoding.Default.GetBytes(password);
            var keyBytes = Encoding.Default.GetBytes(settings.PasswordHashKey);
            var alg = new HMACSHA256(keyBytes);
            var hashedBytes = alg.ComputeHash(originalBytes);
            var hashedPassword = Encoding.Default.GetString(hashedBytes);

            return hashedPassword;
        }

        public string GenerateSalt()
        {
            Random random = new Random();
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < settings.SaltLength; i++)
            {
                sb.Append((char)((int)Math.Floor(93 * random.NextDouble() + 33)));
            }

            return sb.ToString();
        }
    }
}
