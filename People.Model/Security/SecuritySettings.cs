﻿using System.Text.RegularExpressions;
using People.Configuration;

namespace People.Security
{
    public class SecuritySettings : ISecuritySettings
    {
        public SecuritySettings()
        {
            var config = new AppConfig();
            config.Bind("security", this);
        }

        public bool EnforceUsernameFormat { get; set; }

        public string UsernameRegexPattern { get; set; }

        public Regex UsernameRegex => string.IsNullOrEmpty(PasswordRegexPattern) ? null : new Regex(UsernameRegexPattern);

        public bool EnforcePasswordFormat { get; set; }

        public string PasswordRegexPattern { get; set; }

        public Regex PasswordRegex => string.IsNullOrEmpty(PasswordRegexPattern) ? null : new Regex(PasswordRegexPattern);

        public bool HashPasswords { get; set; }

        public bool UseSaltedPasswords { get; set; }

        public byte SaltLength { get; set; }

        public string PasswordHashKey { get; set; }

        public bool LimitPasswordReuse { get; set; }

        public ushort DaysBeforePasswordReuse { get; set; }

        public byte ChangesBeforePasswordReuse { get; set; }

        public bool UseTokens { get; set; }

        public bool TokensExpire { get; set; }

        public string TokenKey { get; set; }

        public string Issuer { get; set; }

        public string Audience { get; set; }

        public int ExpirationTime { get; set; }
    }
}
