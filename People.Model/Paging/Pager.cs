﻿using System;

namespace People.Paging
{
    public class Pager : PagingInfo
    {
        private long? itemCount;
        private int? pageSize;
        private int pageCount;

        public Pager()
        {

        }

        public Pager(long itemCount, int pageNumber, int pageSize, int maxPages)
        {
            PageEntities(itemCount, pageNumber, pageSize, maxPages);
        }

        public override long? ItemCount
        {
            get { return itemCount; }
            set
            {
                itemCount = value;
                GetPageCount();
            }
        }

        public override int? PageNumber { get; set; }

        public override int? PageSize
        {
            get { return pageSize; }
            set
            {
                pageSize = value;
                GetPageCount();
            }
        }

        public override int PageCount
        {
            get
            {
                GetPageCount();
                return pageCount;
            }
        }

        public void PageEntities()
        {
            StartPage = 1;

            GetPageCount();

            if (pageCount > 1)
            {
                if (pageCount <= MaxPages)
                {
                    EndPage = pageCount;
                }
                else
                {
                    var pagesBeforeCurrentPage = (int)Math.Floor((decimal)MaxPages / 2);
                    var pagesAfterCurrentPage = (int)Math.Ceiling((decimal)MaxPages / 2) - 1;

                    if (PageNumber <= pagesBeforeCurrentPage)
                    {
                        EndPage = MaxPages;
                    }
                    else if (PageNumber + pagesAfterCurrentPage >= pageCount)
                    {
                        StartPage = pageCount - MaxPages + 1;
                        EndPage = MaxPages;
                    }
                    else
                    {
                        EndPage = PageNumber.Value + pagesAfterCurrentPage;
                        StartPage = PageNumber.Value - pagesBeforeCurrentPage;
                    }
                }
            }

            ShowFirst = (StartPage > 1);
            ShowPrev = (PageNumber.Value > 1);
            ShowNext = (PageNumber.Value < pageCount);
            ShowLast = (EndPage < pageCount);
        }

        public void PageEntities(long itemCount, int pageNumber, int pageSize, int maxPages)
        {
            this.itemCount = itemCount;
            this.pageSize = pageSize;
            PageNumber = pageNumber;
            MaxPages = maxPages;
            PageEntities();
        }

        public void Reset()
        {
            itemCount = null;
            PageNumber = null;
            pageSize = null;
            MaxPages = 0;
            pageCount = 0;
            StartPage = 0;
            EndPage = 0;
            MaxPages = 0;
            ShowFirst = false;
            ShowPrev = false;
            ShowNext = false;
            ShowLast = false;
        }

        private void GetPageCount()
        {
            if (itemCount.HasValue && pageSize.HasValue)
            {
                if (itemCount > pageSize)
                {
                    pageCount = (int)(itemCount.Value / pageSize.Value);

                    if (itemCount.Value % pageSize.Value > 0)
                    {
                        pageCount++;
                    }
                }
                else
                {
                    pageCount = 1;
                    EndPage = 1;
                }

            }
            else
            {
                pageCount = 0;
            }
        }
    }
}
