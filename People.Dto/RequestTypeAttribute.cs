﻿using System;

namespace People.Dto
{
    public class RequestTypeAttribute : Attribute
    {
        public RequestTypeAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; private set; }
    }
}
