﻿using People.Dto;

namespace People.Dto
{
    [RequestType(RequestTypes.GetPersons)]
    public class GetPeopleRequest : BaseDataRequest
    {
        public GetPeopleRequest()
        {
            orderByColumn = "FirstName";
        }
    }
}
