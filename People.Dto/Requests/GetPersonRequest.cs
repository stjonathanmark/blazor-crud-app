﻿using People.Dto;

namespace People.Dto
{
    [RequestType(RequestTypes.GetPerson)]
    public class GetPersonRequest : BaseRequest
    {
        public int PersonId { get; set; }
    }
}
