﻿using People.Dto;
using System;

namespace People.Dto
{
    [RequestType(RequestTypes.UpdatePerson)]
    public class UpdatePersonRequest : CreatePersonRequest
    {
        public int PersonId { get; set; }
    }
}
