﻿using People.Dto;

namespace People.Dto
{
    [RequestType(RequestTypes.ChangePassword)]
    public class ChangePasswordRequest : BaseRequest
    {
        public string Username { get; set; }

        public string NewPassword { get; set; }

        public string CurrentPassword { get; set; }
    }
}