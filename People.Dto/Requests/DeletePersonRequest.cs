﻿using People.Dto;

namespace People.Dto
{
    [RequestType(RequestTypes.DeletePerson)]
    public class DeletePersonRequest : BaseRequest
    {
        public int PersonId { get; set; }
    }
}
