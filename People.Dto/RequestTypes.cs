﻿namespace People.Dto
{
    public class RequestTypes
    {
        public const string AuthenticateUser = "Authenticate User";
        public const string ChangePassword = "Change Password";
        public const string CreateAccount = "Create Account";

        public const string CreatePerson = "Create Person";
        public const string GetPersons = "Get People";
        public const string GetPerson = "Get Person";
        public const string GetRoles = "Get Roles";
        public const string SearchPersons = "Search People";
        public const string UpdatePerson = "Update Person";
        public const string DeletePerson = "Delete Person";
    }
}
