﻿using People.Paging;

namespace People.Dto
{
    public class BaseDataResponse : BaseResponse
    {
        public BaseDataResponse()
        {

        }

        public BaseDataResponse(BaseResponse response)
            : base(response)
        {

        }

        public BaseDataResponse(BaseDataResponse response)
            : base(response)
        {
            PagingInfo = response.PagingInfo;
        }

        public long Count { get; set; }

        public IPagingInfo PagingInfo { get; set; }
    }
}
