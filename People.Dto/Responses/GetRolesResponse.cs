﻿using System.Collections.Generic;

namespace People.Dto
{
    public class GetRolesResponse : BaseDataResponse
    {
        public IEnumerable<RoleDto> Roles { get; set; }
    }
}
