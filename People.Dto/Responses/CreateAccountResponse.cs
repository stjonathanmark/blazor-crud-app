﻿namespace People.Dto
{
    public class CreateAccountResponse : StatusResponse<UserCreationStatus>
    {
        public bool AccountCreated => Status == UserCreationStatus.Created;
    }
}
