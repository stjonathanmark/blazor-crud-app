﻿using System.Collections.Generic;

namespace People.Dto
{
    public class GetPeopleResponse : BaseDataResponse
    {
        public IEnumerable<PersonDto> People { get; set; }
    }
}
