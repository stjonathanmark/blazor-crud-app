﻿namespace People.Dto
{
    public class AuthenticateUserResponse : StatusResponse<AuthenticationStatus>
    {
        public bool Authenticated => Status == AuthenticationStatus.Authenticated;

        public string Token { get; set; }
    }
}