﻿using People.Configuration;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace People.Dto
{
    public abstract class StatusResponse<TStatus> : BaseStatusResponse<TStatus>
        where TStatus : Enum
    {
        protected AppConfig config;
        protected IDictionary<TStatus, string> statusMessages;

        public StatusResponse()
        {
            config = new AppConfig();
            statusMessages = new Dictionary<TStatus, string>();
            Initialize();
        }

        public override string StatusName => Regex.Replace(Status.ToString(), "(?!(^[A-Z]))([A-Z])", " $2").Trim();

        public override string StatusMessage => statusMessages[Status];

        protected virtual void Initialize()
        {
            config.Bind($"statusMessages:{Status.GetType().Name}", statusMessages);
        }
    }
}
