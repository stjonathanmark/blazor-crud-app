﻿using System.Collections.Generic;

namespace People.Dto
{
    public class BaseResponse
    {

        public BaseResponse()
        {
            Details = new Dictionary<string, object>();
        }

        public BaseResponse(BaseResponse response)
        {
            Successful = response.Successful;
            Message = response.Message;
            Details = response.Details;
        }

        public Dictionary<string, object> Details { get; private set; }

        public bool Successful { get; set; }

        public string Message { get; set; }

        public void SetDetail<TValue>(string key, TValue value)
        {
            Details.Add(key, value);
        }

        public TValue GetDetail<TValue>(string key)
        {
            var value = (TValue)(Details.ContainsKey(key) ? Details[key] : null);
            return value;
        }
    }
}
