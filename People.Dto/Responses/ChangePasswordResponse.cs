﻿namespace People.Dto
{
    public class ChangePasswordResponse : StatusResponse<PasswordChangeStatus>
    {
        public bool PasswordChanged => Status == PasswordChangeStatus.Changed;
    }
}