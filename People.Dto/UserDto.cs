﻿namespace People.Dto
{
    public class UserDto : BaseUser
    {
        public RoleDto Role { get; set; }
    }
}
