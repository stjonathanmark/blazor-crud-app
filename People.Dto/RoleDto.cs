﻿using System.Collections.Generic;

namespace People.Dto
{
    public class RoleDto : BaseRole
    {
        public IList<UserDto> Users { get; set; }
    }
}
