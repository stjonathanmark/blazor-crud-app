﻿using System;

namespace People
{
    public class BaseUser : DataEntity
    {
        public int RoleId { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string Salt { get; set; }

        public DateTime CreationDate { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }
    }
}
