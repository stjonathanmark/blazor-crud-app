﻿namespace People
{
    public enum AuthenticationStatus
    {
        NotAuthenticated,
        UserDoesNotExist,
        InvalidPassword,
        TemporaryPassword,
        PasswordExpired,
        PasswordAlmostExpired,
        NotActivated,
        Disabled,
        AccountLocked,
        Error,
        Authenticated
    }
}
