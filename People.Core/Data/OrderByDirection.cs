﻿namespace People.Data
{
    public class OrderByDirection
    {
        public const string Ascending = "asc";
        public const string Descending = "desc";
    }
}
