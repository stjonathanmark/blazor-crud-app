﻿namespace People
{
    public enum UserCreationStatus
    {
        NotCreated,
        UsernameExists,
        InvalidUsernameFormat,
        InvalidPasswordFormat,
        Error,
        Created
    }
}
