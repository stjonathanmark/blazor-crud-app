﻿namespace People
{
    public class BaseRole : DataEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
