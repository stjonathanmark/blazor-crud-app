﻿namespace People
{
    public class DataEntity<TIdentity> : Entity<TIdentity>
        where TIdentity : struct
    {
        public bool Deleted { get; set; }
    }

    public class DataEntity : DataEntity<int>
    { }
}
