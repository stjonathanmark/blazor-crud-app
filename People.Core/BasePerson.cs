﻿using System;

namespace People
{
    public class BasePerson : DataEntity
    {
        public string Prefix { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string Suffix { get; set; }

        public DateTime BirthDate { get; set; }

        public string Gender { get; set; }
    }
}
