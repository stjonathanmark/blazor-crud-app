﻿using Microsoft.Extensions.Configuration;

namespace People.Configuration
{
    public class AppConfig
    {
        private readonly IConfiguration config;

        public AppConfig()
        {
            config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();
        }

        public TConfigSection GetSection<TConfigSection>(string sectionName)
            where TConfigSection : new()
        {
            var configSection = new TConfigSection();
            Bind(sectionName, configSection);
            return configSection;
        }

        public IConfigurationSection GetSection(string sectionName)
        {
            return config.GetSection(sectionName);
        }

        public void Bind<TConfigSection>(string sectionName, TConfigSection section)
        {
            config.GetSection(sectionName).Bind(section);
        }

        public T Get<T>(string key)
        {
            return config.GetValue<T>(key);
        }
    }
}
