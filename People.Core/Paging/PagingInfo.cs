﻿namespace People.Paging
{
    public class PagingInfo : IPagingInfo
    {
        public virtual long? ItemCount { get; set; }

        public virtual int? PageNumber { get; set; }

        public virtual int? PageSize { get; set; }

        public virtual int PageCount { get; }

        public virtual int StartPage { get; protected set; }

        public virtual int EndPage { get; protected set; }

        public virtual int MaxPages { get; protected set; }

        public virtual int SidePages { get; protected set;  }

        public virtual int? PrevPage => PageNumber.HasValue && PageNumber.Value > 1 ? PageNumber - 1 : null;

        public virtual int? NextPage => PageNumber.HasValue && PageNumber.Value < EndPage ? PageNumber + 1 : null;

        public virtual bool ShowFirst { get; protected set; }

        public virtual bool ShowPrev { get; protected set; }

        public virtual bool ShowNext { get; protected set; }

        public virtual bool ShowLast { get; protected set; }
    }
}
