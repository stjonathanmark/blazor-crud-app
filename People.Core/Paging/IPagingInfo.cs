﻿namespace People.Paging
{
    public interface IPagingInfo
    {
        int EndPage { get; }
        long? ItemCount { get; set; }
        int MaxPages { get; }
        int? NextPage { get; }
        int PageCount { get; }
        int? PageNumber { get; set; }
        int? PageSize { get; set; }
        int? PrevPage { get; }
        bool ShowFirst { get; }
        bool ShowLast { get; }
        bool ShowNext { get; }
        bool ShowPrev { get; }
        int SidePages { get; }
        int StartPage { get; }
    }
}