# Blazor CRUD App Demo
This is a demo Blazor CRUD web app that was created with ASP.Net Core 3.1, EntityFramework Core 3.1.7, and a Blazor Web Assembly frontend.

## Instructions to run the application in your local environment using Visual Studio 2019
1. Replace the string value being assigned to the **"ConnectionStrings:DataContext"** property on **"line 15"** in the [appsettings.json file](https://bitbucket.org/stjonathanmark/blazor-crud-app/src/master/People.Web.Api/appsettings.json) in the root of the **"People.Web.Api"** project with the connection string for the test database server and the name of the database to be created using migration in next step. 
2. Make **"People.Data"** the start up project, and go to the **"Package Manager Console"** and type and run the command **"Update-Database"** to create the database.
3. Make both **"People.Web"** and **"People.Web.Api"** the start up projects and press **"F5"** to start the application.
4. Two browsers will pop up, one with the Swagger UI for the web API and another with the Blazor UI for the main application
5. Add the following two users (one at a time) through the Swagger UI with the "POST /api/Account" request tab:

        {
            "roleId": 1,
            "username": "admin",
            "password": "Password1!",
            "firstName": "John",
            "lastName": "Doe",
            "email": "john@doe.com"
        }

        {
            "roleId": 2,
            "username": "user",
            "password": "Password1!",
            "firstName": "Jim",
            "lastName": "Doe",
            "email": "jim@doe.com"
        }
        
6. Login as either of the two users through the Blazor UI
7. Enjoy!!!!!!!