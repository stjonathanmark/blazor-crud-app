﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using People.Security;

namespace People.Data
{
    public static class DataMapper
    {
        public static void Map(EntityTypeBuilder<Person> entity, string table = "Persons", string schema = "dbo")
        {
            entity.ToTable(table, schema);
            entity.HasKey(p => p.Id);

            entity.Property(p => p.Prefix).HasMaxLength(50);
            entity.Property(p => p.FirstName).HasMaxLength(50).IsRequired();
            entity.Property(p => p.MiddleName).HasMaxLength(50);
            entity.Property(p => p.LastName).HasMaxLength(50).IsRequired();
            entity.Property(p => p.Suffix).HasMaxLength(15);
            entity.Property(p => p.Gender).HasMaxLength(6).IsRequired();
        }

        public static void Map(EntityTypeBuilder<Role> entity, string table = "Roles", string schema = "dbo")
        {
            entity.ToTable(table, schema);
            entity.HasKey(r => r.Id);
            entity.Property(r => r.Name).IsRequired().HasMaxLength(50);
            entity.Property(r => r.Description).IsRequired().HasMaxLength(1024);

            entity.HasMany(r => r.Users).WithOne(u => u.Role).HasForeignKey(u => u.RoleId).OnDelete(DeleteBehavior.Restrict);

            entity.HasData(new Role[]
            {
                new Role { Id = 1, Deleted = false, Name = "Administrator", Description = "Role that can do all actions." },
                new Role { Id = 2, Deleted = false, Name = "User", Description = "Role that can do some actions." }
            });
        }

        public static void Map(EntityTypeBuilder<User> entity, string table = "Users", string schema = "dbo")
        {
            entity.ToTable(table, schema);
            entity.HasKey(u => u.Id);

            entity.Property(u => u.Username).HasMaxLength(50).IsRequired();
            entity.Property(u => u.Password).HasMaxLength(1024).IsRequired();
            entity.Property(u => u.Salt).HasMaxLength(256).IsRequired();
            entity.Property(u => u.Email).HasMaxLength(50).IsRequired();
            entity.Property(u => u.FirstName).HasMaxLength(50).IsRequired();
            entity.Property(u => u.LastName).HasMaxLength(50).IsRequired();
        }
    }
}
